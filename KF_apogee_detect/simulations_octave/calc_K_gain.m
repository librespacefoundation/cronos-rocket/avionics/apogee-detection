function [K,P,H,A] = calc_K_gain(dt)
% The Default Time Step is 50 ms
if nargin 
  T = dt; % time step (in s)
else
  T = 0.05; % time step (10 ms)
end
%% Define Model Parameters 

sigma_p = 4.1667;% altimeter sigma in meters (p.3 BMP384 Datasheet - absolute accuracy)
%sigma_p = 5.9852;
varience_p = sigma_p*sigma_p; % altimeter varience 
sigma_a = 0.5883989;% accelerometer sigma in m/s^2 (p.14 LSM9DS0 Datasheet)
%sigma_a = 0.0346;
varience_a = sigma_a*sigma_a; % accelerometer varience 

%% Define System Parameters 

H = [1 0 0; 0 0 1]; % measurements matrix ➔ maps z (sensor data) to x (state variables) 
R = [varience_p 0; 0 varience_a]; % measurement noise covariance
Q = [0 0 0; 0 0 0; 0 0 1]; % process noise covariance matrix

A = [1, T, 1/2*T^2; 0, 1, T; 0, 0, 1]; % dynamic model matrix ➔ maps x_{k} to x_{k-1}

%% Calculate the Kalman gain

% These three equations recursively define K (matrix of kalman gains) and P (error covariance matrix)

P = eye(3); % initial guess for P

for i = 1:10000
    K = P*H'/(H*P*H' + R); % Kalman gains
    P = (eye(3) - K *H)*P;
    P = A*P*A' + Q;
end

% display(K)
% display(H)
% display(P)
end