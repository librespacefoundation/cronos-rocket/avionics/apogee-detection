%AGL = Altitude above Ground
%ASL = ALtitude Above Sea level

%% Raven's Real Rocket Data(Waterloo)
Data = dlmread('RavenRocketRealLaunchData.csv',',',1,0);

t_a_a = Data(:,1);
a_a = 9.81*Data(:,2);
t_a_l = Data(1:43519,3);
a_l = 9.81*Data(1:43519,4);
t_alt_AGL = Data(1:4351,5);
alt_AGL =  0.3048*Data(1:4351,6); #in meters



##figure(1)
##plot(t_alt_AGL,alt_AGL)
##xlabel('t(s)')
##ylabel('altitude(feet)')
##grid on
##figure(2)
##plot(t_a_a(1:20000),a_a(1:20000))
##hold on
##plot(t_a_l,a_l)
##xlabel('t')
##ylabel('a in m/s')
##axis on
##legend("Axial acceleration","Lateral acceleration")
##title('Raven Real Flight Data')





%% Interpolate Data
dt = 0.05;
t = (0.1:dt:t_alt_AGL(end))';
a_lateral = interp1(t_a_l, a_l, t);
a_axial = interp1(t_a_a, a_a, t);
a = sqrt(a_lateral.^2 + a_axial.^2);
s = interp1(t_alt_AGL, alt_AGL, t);
export_real_data = [t, s, a];
##figure(1)
##n = 1000;
##plot(t(1:n),a(1:n),"k",'LineWidth', 2)
##hold on
##plot(t(1:n),a_lateral(1:n),'LineWidth', 1.1,"g")
##plot(t(1:n),a_axial(1:n),'LineWidth', 1.1,"b")
##xlabel('t')
##ylabel('a in m/s')
##
##line1 = zeros(length(t(1:n)),1);
##line2 = 10*ones(length(t(1:n)),1);
##plot(t(1:n),line1, 'LineWidth',1.1,"r")
##plot(t(1:n),line2, 'LineWidth', 1.1,"r")
##xlim([0, t(n)])
##ylim([-3, max(a(1:n))+5])
##grid on
##text(20,18,'Coast Phase','Color','red','FontSize',20)
##legend("a = sqrt(a_l^2+a_a^2)", "lateral acceleration", "axial acceleration",'0 m/s','10 m/s')
##hold off



%% Implements Kalman filter on altitude and accelerometer data. Required vectors are alt and accel
estimate = zeros(3,length(t));
estimate = zeros(3,2);
ds = zeros(length(t),1);
v1 = 0; % TO DO: differentiate
estimate(:,1) = [s(1); v1; a(1)];
[K,P,H,A] = calc_K_gain(dt);
Acceleration_measurement = 1;

K = [0.0262, 0.0004;
    0.0070, 0.0107;
    0.0000, 0.7861];

A = [1, 0.05, 0.0013;
		   0, 1.0000, 0.0500;
				  0, 0, 1.0000];

H = [1, 0, 0;
    0, 0, 1];

for i = 2:length(t)

estimate(1,i) =  A(1,1) * estimate(1,i-1) + A(1,2) *estimate(2,i-1) +A(1,3) *estimate(3,i-1);
estimate(2,i) =  A(2,1) * estimate(1,i-1) + A(2,2) *estimate(2,i-1) +A(2,3) *estimate(3,i-1);
estimate(3,i) =  A(3,1) * estimate(1,i-1) + A(3,2) *estimate(2,i-1) +A(3,3) *estimate(3,i-1);
estimate(1,i) = estimate(1,i) +  K(1,1)*(s(i) - H(1,1)*estimate(1,i) - H(1,2)*estimate(2,i) - H(1,3)*estimate(3,i)) + K(1,2)*(a(i) - H(2,1)*estimate(1,i) - H(2,2)*estimate(2,i) - H(2,3)*estimate(3,i));
estimate(2,i) = estimate(2,i) +  K(2,1)*(s(i) - H(1,1)*estimate(1,i) - H(1,2)*estimate(2,i) - H(1,3)*estimate(3,i)) + K(2,2)*(a(i) - H(2,1)*estimate(1,i) - H(2,2)*estimate(2,i) - H(2,3)*estimate(3,i));
estimate(3,i) = estimate(3,i) +  K(3,1)*(s(i) - H(1,1)*estimate(1,i) - H(1,2)*estimate(2,i) - H(1,3)*estimate(3,i)) + K(3,2)*(a(i) - H(2,1)*estimate(1,i) - H(2,2)*estimate(2,i) - H(2,3)*estimate(3,i));
ds(i) = estimate(1,i) - estimate(1,i-1);
end



figure(3);
plot(t_alt_AGL,alt_AGL, 'b') % Plot actual simulated position
hold on
plot(t,(estimate(1,:))', 'r') % Plot Kalman Filtered Position

[apogee,t_index] = max(s); % Determine and display actual apogee
disp('Actual Apogee occurs at t =')
disp(t(t_index))
[Kalman_apogee,Kalman_t_index] = max((estimate(1,:))); % Determine and display actual apogee
%
 disp('Estimated Kalman Apogee occurs at t =')
 disp(t(Kalman_t_index))
%
disp('Error  =') %% Error is randomized because the seed is randomized
 % You can run the simulation multiple times and you will get different results
 % Across 200 flights with randomized parameters, the Kalman Filter displayed a mean error in detecting apogee of 0.269 seconds
 %(Ref: Digital Detection of Rocket Apogee,Evan Dougal etc.,Rice University, Houston, Texas)
%
% disp(t(t_index) - t_k(1925))
% text(15, 1000, sprintf('Error  = %.4f s ', t(t_index) - t_k(Kalman_t_index)))
legend("Pressure Data", "Kalman Filter")
% xlim([t(index),150])
% hold off
