A = dlmread('Open_Rocket_final.csv',',',1,0);

# 'Actual' Data (Open Rocket Simulation)
t = A(:,1);
s = A(:,2);
u_v = A(:,3);
a_v = A(:,4);

##figure
##plot(t,u_v)
##hold on
##plot(t,s)
##hold on
##plot(t,a_v)
##xlabel('t')
##ylabel('data')
##title('Rocket Simulated Flight Data')

## Add Gaussian sensor noise
# Position corrupted with Gaussian noise with standard deviation = 4.1667
s_n = s +(4.1667.*randn(1,length(s)))';

# Velocity corrupted with Gaussian noise with standard deviation = 2
v_n = u_v+(2.*randn(1,length(t)))';

# Position corrupted with Gaussian noise with standard deviation = 0.5883989
a_n = a_v+(0.5883989.*randn(1,length(t)))';

## figure;
## plot(t,s_n) % Plot noisy position
## title('Noisy Position')
## figure;
## plot(t,v_n) % Plot noisy velocity
## title('Noisy Velocity')
## figure;
## plot(t,a_n) % Plot noisy acceleration
## title('Noisy Acceleration')

## Add Quantization

## Time Quantized to 0.01 seconds

## Amplitude quantization is 3 meters (example)

t_q = (0.01:0.01:221)';
yy1 = interp1(t, s_n, t_q);
yy2 = interp1(t, v_n, t_q);
yy3 = interp1(t, a_n, t_q);
s_n_q = floor(yy1./3).*3;
v_n_q = floor(yy2./3).*3;
a_n_q = floor(yy3./3).*3;

## figure;
## plot(t_q,s_n_q) % Plot noisy and quantized position
## title('Noisy and Quantized Position')
## figure;
## plot(t_q,v_n_q) % Plot noisy and quantized velocity
## title('Noisy and Quantized Velocity')
## figure;
## plot(t_q,a_n_q) % Plot noisy and quantized acceleration
## title('Noisy and Quantized Acceleration')


## Implement Kalman FIlter
## when acc < threshold start implementing Kalman FIlter
threshold = 300;
index = find(a_n_q < threshold,1);

t_q = (t(index):0.01:221)';
yy1 = interp1(t(index:end,1), s_n(index:end,1), t_q);
yy2 = interp1(t, v_n, t_q);
yy3 = interp1(t, a_n, t_q);
s_n_q = floor(yy1./3).*3;
v_n_q = floor(yy2./3).*3;
a_n_q = floor(yy3./3).*3;

## implements Kalman filter on altitude and accelerometer data. Required vectors are alt and accel

dt = 0.01;
t_k = (t(index):dt:221)';
estimate = zeros(3,length(t_k));
estimate(:,1) = [s_n_q(1); v_n_q(1); a_n_q(1)];
[K,P,H,A] = calc_K_gain(dt);
for i = 2:length(t_k)
estimate(:,i) = A*estimate(:,i-1);
estimate(:,i) = estimate(:,i) + K*([s_n_q(i);a_n_q(i)] - H *estimate(:,i));
end

figure(1)
plot(t(index:end,1),s(index:end,1), LineWidth=2, 'r') % Plot actual simulated position
hold on
plot(t_q,s_n_q, LineWidth=2, 'b') % Plot noisy and quantized position
plot(t_k,(estimate(1,:))',LineWidth=2, 'green') % Plot Kalman Filtered Position

[apogee,t_index] = max(s); % Determine and display actual apogee
disp('Actual Apogee occurs at t =')
disp(t(t_index))
[Kalman_apogee,Kalman_t_index] = max((estimate(1,:))); % Determine and display actual apogee

disp('Estimated Kalman Apogee occurs at t =')
disp(t_k(Kalman_t_index))

disp('Error  =') ## Error is randomized because the seed is randomized
## You can run the simulation multiple times and you will get different results
## Across 200 flights with randomized parameters, the Kalman Filter displayed a mean error in detecting apogee of 0.269 seconds
##(Ref: Digital Detection of Rocket Apogee,Evan Dougal etc.,Rice University, Houston, Texas)

disp(t(t_index) - t_k(1925))
text(15, 1000, sprintf('Error  = %.4f s ', t(t_index) - t_k(Kalman_t_index)))
legend("Actual Data", "Sensor Data", "Kalman Filter")
xlim([t(index),150])
hold off

