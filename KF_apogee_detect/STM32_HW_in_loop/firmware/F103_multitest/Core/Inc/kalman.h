/*
 * kalman.h
 *
 */

/******************************************************************************
DESCRIPTION:
    This functions performs Apogee Detection using
    a linear(or constant acceleration) Kalman
    filtering model.The measurements involve
    acceleration and altitude.

AUTHOR
    Victoria Malyshkina

REPORTING BUGS AND REVISIONS
    mailto:victoriam2000@mail.ru
*******************************************************************************/

#include <stdbool.h>
#ifndef INC_KALMAN_H_
#define INC_KALMAN_H_


/**
  * @brief  Returns current estimation (altitude, velocity, acceleration) given the measurements and the previous KF estimation
  * @param  Current altitude scalar
  * @param  Current acceleration scalar
  * @param  Previous estimation of the Kalman Filter( a pointer to a 3X1 array)
  * @retval Current estimation of the Kalman Filter( a pointer to a 3X1 array)
  */


float * KalmanFilter (float altitude, float acceleration, float est_prev[]);

/**
  * @brief  Measures how many times the current altitude is smaller than the previous one
  * @param  Current altitude KF estimation
  * @param  Previous altitude KF estimation
  * @retval Counter based on which the CheckIfApogee() function decides if apogee occurred or not
  */

int DerivativeCounter (float current_KF_alt, float prev_KF_alt);

/**
  * @brief  Return True if apogee event has occurred, else returns False
  * @param  retval of DerivativeCounter()
  * @retval True or False value that is needed in order to eject the parachutes
  */

bool CheckIfApogee (int i);


#endif /* INC_KALMAN_H_ */
