/*
 * kalman.c
 *
 */



/*
// To use these functions run the following code in main.c:

  float est[3];
  float est_prev[3] = { 0.1, 0.1, 0.1};
  float *estimate;
  int i;
  bool Apogee;
  float Altitude_measurement = 389; // in meters(for example)
  float Acceleration_measurement = 1; // in m/s^2(for example)
  estimate = KalmanFilter (Altitude_measurement, Acceleration_measurement, est_prev);
  float altitude_current_est = *(estimate);
  i = DerivativeCounter(altitude_current_est, est_prev[0]);
  // Every 50 ms check is Apogee has been reached 
  Apogee = CheckIfApogee (i);
  printf ("Apogee is %d or ", Apogee);
  fputs (Apogee ? "true" : "false\n", stdout);
  printf ("Altitude %f \n", altitude_current_est);
*/

#include <kalman.h>
#include <stdbool.h>
int DerivativeCounter (float current_KF_alt, float prev_KF_alt)
{
  int i;
  float ds;
  ds = current_KF_alt - prev_KF_alt;
  i = 0;
  if (ds <= 0)
    i = +1;
  else
    i = 0;
  return i;
}

bool CheckIfApogee (int i)
{

  if (i > 10)
    return true;
  else
    return false;
}



float * KalmanFilter (float altitude, float acceleration, float est[])
{
  extern float est[3];

  //float dt = 0.05;
  float Kgain[3][2] = { {0.0262, 0.0004},
    {0.0070, 0.0107},
    {0.0000, 0.7861}
  };


  float A[3][3] = { {1, 0.05, 0.0013},
		  { 0, 1.0000, 0.0500},
				  {0, 0, 1.0000}
  };

  float H[2][3] = { {1, 0, 0},
    {0, 0, 1}
  };

  est_prev[0] =  A[0][0] * est[0] + A[0][1] * est[1] + A[0][2] * est[2];
  est_prev[1] =  A[1][0] * est[0] + A[1][1] * est[1] + A[1][2] * est[2];
  est_prev[2] =  A[2][0] * est[0] + A[2][1] * est[1] + A[2][2] * est[2];

  est[0] =  est_prev[0] + Kgain[0][0] * (altitude - H[0][0] * est_prev[0] - H[0][1] * est_prev[1] - H[0][2] * est_prev[2]) + Kgain[0][1] * (acceleration - H[1][0] * est_prev[0] - H[1][1] *	est_prev[1] - H[1][2] * est_prev[2]);
  est[1] =  est_prev[1] + Kgain[1][0] * (altitude - H[0][0] * est_prev[0] - H[0][1] * est_prev[1] - H[0][2] * est_prev[2]) + Kgain[1][1] * (acceleration - H[1][0] * est_prev[0] - H[1][1] * est_prev[1] -H[1][2] * est_prev[2]);
  est[2] =  est_prev[2] + Kgain[2][0] * (altitude - H[0][0] * est_prev[0] - H[0][1] * est_prev[1] -H[0][2] * est_prev[2]) + Kgain[2][1] * (acceleration -H[1][0] * est_prev[0] -H[1][1] * est_prev[1] -H[1][2] * est_prev[2]);

  return est;
}

