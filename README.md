# Apogee Detection algorithms and test
To verify our software, a variety of test cases are run. These are data profiles that automatically generated with  OpenRocket simulator for random conditions using python scripting. Then these profiles are passed into the Data Logger software as if they were sensor data, and the software's response is observed. 

# Kalman Filter implementation and testing
## Octave simulations
Under the `simulations_octave` one can find some Octave simulations of the Kalman filter that is goind to be used.  

## STM32 HW in-the-loop testing
Under `STM32_HW_in_loop` directory, one can locate firmware for STM32 (in this case, applied to a Nucleo-F103RB board) as well as a python script used to feed data to the embedded system.   

Data aquisition from sensors is simulated via an incoming datastream from UART. Data are handled via DMA with ILDE line detection, which allows us to receive data packets of unknown size, as long as we have a long enough buffer to store them. All functions (data formatting, and Kalman filtering) are performed in the UART_RX interrupt, so in the main while() loop, one can only see a small delay.   
At this point, to visualise the STM32 response to the incoming data, we are using CubeMonitor. In case one wants to transmit data through UART, it is suggested that they use DMA to do so, since UART_TX is a blocking action. 

   
### **Using the Python data feeder script**  
**Dependencies**   
First, one has to install `pyserial` (NOT `serial`) with 
```
pip install pyserial
```
In case `serial` is already installed in your system, before installing `pyserial`, it has to be removed with 
``` 
pip uninstall serial
```

Also `time`and `pandas` are needed and `matplotlib` can help with debugging plots. 


# Open Rocket automated simulation generator
In orded not to upload binary application files to git, one should have the `OpenRocket_vxx.jar` file in the `sw_verification/open_rocket/` directory. 
